
# Custom Form Controls

The Angular project includes two form controls implemented from scratch. To achieve this, both controls implement ControlValueAccessor.

**Live Demo:** [Custom Form Controls](https://custom-form-control.vercel.app)

## Getting started

Make sure you have the Angular CLI installed.  

To manage dependencies you can use yarn or npm.  
Depending on what you choose, run `yarn install` or `npm install`.

Once all dependencies are resolved run `ng serve` for a dev server.  
Then open your browser and navigate tn `http://localhost:4200/`.

## Functionality overview

### Multi Select Input
* Dropdown panel with animations
* Fully keyboard-accessible (arrows, Enter, ESC) for enhanced usability
* Customizing icons for options (checked and unchecked states)
* Free to choose which property indicates that an option is selected
* Flexibility in specifying the applied filter
* Passing a function to be called when an option is touched
* Passing a function to be called when the panel is opened/closed
* Adjust the height of the panel

### Tags Input
* Input for adding new tags
* Flexibility in defining the applied styles for the input, button and tag elements
* Customizing the icon for removing a tag
* Passing a function to be called when the tag list is changed

<div>
  <p align="center">
    <img src="src/assets/images/angular_logo.png" alt="angular-logo" width="120px" height="120px"/>
  </p>
</div>
