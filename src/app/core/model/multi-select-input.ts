export class MultiSelectOption {
  constructor(
    public id: number,
    public value: string,
    public done: boolean = false,
  ){}
}
