import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'multi-select-input',
    loadComponent:
      () => import('./use-multi-select-input/use-multi-select-input.component')
        .then(({ UseMultiSelectInputComponent }) => UseMultiSelectInputComponent)
  },
  {
    path: 'tags-input',
    loadComponent:
      () => import('./use-tags-input/use-tags-input.component')
        .then(({ UseTagsInputComponent }) => UseTagsInputComponent)
  },
  {
    path: '**',
    loadComponent:
    () => import('./use-multi-select-input/use-multi-select-input.component')
      .then(({ UseMultiSelectInputComponent }) => UseMultiSelectInputComponent)
  }
];
