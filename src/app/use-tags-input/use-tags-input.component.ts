import { ChangeDetectionStrategy, Component } from "@angular/core";
import { TagsInputComponent } from "../controls/tags-input/tags-input.component";


@Component({
  selector: 'cc-use-tags-input',
  standalone: true,
  imports: [
    TagsInputComponent,
  ],
  templateUrl: './use-tags-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UseTagsInputComponent {
  handleTags(tags: string[]) {
    console.log('Tags: ', tags);
  }
}
