import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TagsInputComponent } from './tags-input.component';
import { ChangeDetectorRef } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';


describe('TagsInputComponent', () => {
  let component: TagsInputComponent;
  let fixture: ComponentFixture<TagsInputComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        TagsInputComponent,
        NoopAnimationsModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsInputComponent);
    component = fixture.debugElement.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a tag', () => {
    const tagValue = 'Test Tag';
    component.inputValue = tagValue;
    component.addTag();
    expect(component._tags).toContain(tagValue);
  });

  it('should remove a tag', () => {
    const tagValue = 'Test Tag';
    component._tags = [tagValue, 'Another Tag'];
    component.removeTag(0);
    expect(component._tags).not.toContain(tagValue);
  });

  it('should emit the updated tag list', () => {
    spyOn(component.tags, 'emit');
    const tagValue = 'Test Tag';
    component.inputValue = tagValue;

    component.addTag();

    expect(component.tags.emit).toHaveBeenCalledWith([tagValue]);
  });

  describe('check style setting', () => {
    describe('check input', () => {
      it('should apply inputBorder style', () => {
        const inputValue = '2px solid red';
        component.inputBorder = inputValue;
        getChangeDetection(fixture).detectChanges();
        const inputElement = getInput(fixture);
        expect(inputElement.style.border).toBe(inputValue);
      });

      it('should apply inputOutlineColor style', () => {
        const inputValue = 'rgba(2, 5, 3, 0.2)';
        component.inputOutlineColor = inputValue;
        getChangeDetection(fixture).detectChanges();
        const inputElement = getInput(fixture);
        expect(inputElement.style.outlineColor).toBe(inputValue);
      });
    })

    describe('check button', () => {
      it('should apply buttonBorder style', () => {
        const inputValue = '2px solid rgb(17, 115, 2)';
        component.buttonBorder = inputValue;
        getChangeDetection(fixture).detectChanges();
        const buttonElement = getButton(fixture);
        expect(buttonElement.style.border).toBe(inputValue);
      });

      it('should set buttonHoverFillColor property', () => {
        const hoverFillColor = 'rgb(207, 117, 0)';
        component.buttonHoverFillColor = hoverFillColor
        getChangeDetection(fixture).detectChanges();

        const buttonElement = getButton(fixture);
        const computedStyles = getComputedStyle(buttonElement);
        const hoverfillColorProperty = computedStyles.getPropertyValue('--buttonHoverFillColor');

        expect(hoverfillColorProperty).toBe(hoverFillColor);
      });

      it('should set buttonTextColor property', () => {
        const buttonTextColor = 'yellow';
        component.buttonTextColor = buttonTextColor
        getChangeDetection(fixture).detectChanges();

        const buttonElement = getButton(fixture);
        const computedStyles = getComputedStyle(buttonElement);
        const hoverTextColorProperty = computedStyles.getPropertyValue('--buttonTextColor');

        expect(hoverTextColorProperty).toBe(buttonTextColor);
      });

      it('should set buttonHoverTextColor property', () => {
        const hoverTextColor = 'blue';
        component.buttonHoverTextColor = hoverTextColor
        getChangeDetection(fixture).detectChanges();

        const buttonElement = getButton(fixture);
        const computedStyles = getComputedStyle(buttonElement);
        const hoverTextColorProperty = computedStyles.getPropertyValue('--buttonHoverTextColor');

        expect(hoverTextColorProperty).toBe(hoverTextColor);
      });
    })

    describe('tagBorder', () => {
      beforeEach(() => {
        component.inputValue = 'Test Tag';
        component.addTag();
      });

      it('should apply tagBorder style', () => {
        const inputValue = '1px solid yellow';
        component.tagBorder = inputValue;

        getChangeDetection(fixture).detectChanges();
        const tagElement = getTagElement(fixture);

        expect(tagElement.style.border).toBe(inputValue);
      })

      it('should apply tagFillColor style', () => {
        const inputValue = 'green';
        component.tagFillColor = inputValue;

        getChangeDetection(fixture).detectChanges();
        const tagElement = getTagElement(fixture);

        expect(tagElement.style.backgroundColor).toBe(inputValue);
      })

      it('should apply tagTextColor style', () => {
        const inputValue = 'blue';
        component.tagTextColor = inputValue;

        getChangeDetection(fixture).detectChanges();
        const tagElement = getTagElement(fixture);

        expect(tagElement.style.color).toBe(inputValue);
      })

      it('should apply removeEmoji', () => {
        const inputValue = '✖️';
        component.removeEmoji = inputValue;

        getChangeDetection(fixture).detectChanges();
        const tagElement = getRemoveTagElement(fixture);

        expect(tagElement.textContent?.trim()).toBe(inputValue);
      })
    })
  })

});


const getChangeDetection = <T>(fixture: ComponentFixture<T>) => (
  fixture.debugElement.injector.get<ChangeDetectorRef>(ChangeDetectorRef)
)


const getInput = <T>(fixture: ComponentFixture<T>) => (
  getSelector(fixture, 'input') as HTMLInputElement
)

const getButton = <T>(fixture: ComponentFixture<T>) => (
  getSelector(fixture, 'button') as HTMLButtonElement
)

const getTagElement = <T>(fixture: ComponentFixture<T>) => (
  getSelector(fixture, '.tag-name') as HTMLSpanElement
)

const getRemoveTagElement = <T>(fixture: ComponentFixture<T>) => (
  getSelector(fixture, '.remove-tag') as HTMLSpanElement
)


const getSelector = <T>(fixture: ComponentFixture<T>, selector: string) => (
  fixture.debugElement.nativeElement.querySelector(selector)
)
