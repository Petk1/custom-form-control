import { animate, state, style, transition, trigger } from "@angular/animations";
import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";
import { ControlValueAccessor, FormsModule, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
  selector: 'cc-tags-input',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
  ],
  templateUrl: './tags-input.component.html',
  styleUrls: ['./tags-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TagsInputComponent,
      multi: true
    }
  ],
  animations: [
    trigger('fadeIn', [
      state('void', style({ opacity: 0 })),
      transition(':enter', animate('200ms', style({ opacity: 1 }))),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsInputComponent implements ControlValueAccessor {
  @Input() inputBorder: string | undefined;
  @Input() inputOutlineColor: string | undefined;
  @Input() buttonBorder: string | undefined;
  @Input() buttonHoverFillColor: string | undefined;
  @Input() buttonTextColor: string | undefined;
  @Input() buttonHoverTextColor: string | undefined;
  @Input() tagBorder: string | undefined;
  @Input() tagFillColor: string | undefined;
  @Input() tagTextColor: string | undefined;
  @Input() removeEmoji: string = '❌';

  @Output() tags = new EventEmitter<string[]>();

  _tags: string[] = [];
  inputValue: string = '';

  protected onChange: (value: string[]) => void = () => {};
  protected onTouch: () => void = () => {};

  writeValue(tags: string[]): void {
    this._tags = tags;
  }

  registerOnChange(fn: (value: string[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  addTag(): void {
    const value = this.inputValue.trim();
    if(!value) return;

    this._tags.push(value);
    this.inputValue = '';
    this.onChange(this._tags);
    this.emitTags();
  }

  removeTag(index: number): void {
    this._tags.splice(index, 1);
    this.onChange(this._tags);
    this.emitTags();
  }

  private emitTags() {
    this.tags.emit(this._tags);
  }
}
