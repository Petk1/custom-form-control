import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, EventEmitter, Output, QueryList } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MultiSelectInputComponent } from './multi-select-input.component';
import { MultiSelectOption } from 'src/app/core/model/multi-select-input';

@Component({
  selector: 'form-option',
  template: '<div></div>'
})
class MockOptionComponent<T> {
  @Output() selected = new EventEmitter<MockOptionComponent<T>>();
  toggleSelect = jasmine.createSpy();
  scrollIntoThisOption = jasmine.createSpy();
  setActiveStyles = jasmine.createSpy();
  setInactiveStyles = jasmine.createSpy();
}


describe('MultiSelectInputComponent', () => {
  let component: MultiSelectInputComponent<MultiSelectOption>;
  let fixture: ComponentFixture<MultiSelectInputComponent<MultiSelectOption>>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        MultiSelectInputComponent,
        NoopAnimationsModule,
      ],
      declarations: [
        MockOptionComponent
      ]
    }).compileComponents();
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectInputComponent<MultiSelectOption>);
    component = fixture.debugElement.componentInstance;

    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set isOpenPanel to true when clicked first time', () => {
    component.openPanel();
    expect(component.isOpenPanel).toBe(true);
  });

  it('should set isOpenPanel to false when closePanel() is called', () => {
    component.closePanel();
    expect(component.isOpenPanel).toBe(false);
  })

  it('should set blur on input when closePanel() is called', () => {
    const nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    const blurSpy = spyOn(
      nativeElement.querySelector('input') as HTMLInputElement,
      'blur'
    );

    component.closePanel();

    expect(blurSpy).toHaveBeenCalledTimes(1);
  })

  it('should call onTouched() when focusout event occurs', () => {
    // @ts-expect-error
    spyOn(component, 'onTouched');

    fixture.debugElement.triggerEventHandler('focusout', null);

    // @ts-expect-error
    expect(component.onTouched).toHaveBeenCalled();
  })

  it('should set focus on input when markAsTouched() is called and isOpenPanel is true', () => {
    const nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    const focusSpy = spyOn(
      nativeElement.querySelector('input') as HTMLInputElement,
      'focus'
    );
    component.isOpenPanel = true;

    component.markAsTouched();

    expect(focusSpy).toHaveBeenCalledTimes(1);
  })

  it('should not call onTouched() when markAsTouched() is called and disabled is true', () => {
    // @ts-expect-error
    spyOn(component, 'onTouched');
    component.disabled = true;

    component.markAsTouched();

    // @ts-expect-error
    expect(component.onTouched).not.toHaveBeenCalled();
  })

  it('should call openPanel() when down arrow key is pressed and isOpenPanel is false', () => {
    const openPanelSpy = spyOn(component, 'openPanel');
    component.isOpenPanel = false;

    fixture.debugElement.triggerEventHandler('keydown', { key: 'ArrowDown' } as KeyboardEvent);

    expect(openPanelSpy).toHaveBeenCalledTimes(1);
  })

  it('should call onKeydown() when down or up arrow key is pressed and isOpenPanel is true', () => {
    // @ts-expect-error
    const keyManagerSpy = spyOn(component.keyManager, 'onKeydown');
    component.isOpenPanel = true;

    fixture.debugElement.triggerEventHandler('keydown', { key: 'ArrowDown' } as KeyboardEvent);
    fixture.debugElement.triggerEventHandler('keydown', { key: 'ArrowUp' } as KeyboardEvent);

    expect(keyManagerSpy).toHaveBeenCalledTimes(2);
  })

  it('should call toggleSelect() when enter key is pressed and isOpenPanel is true', () => {
    component.isOpenPanel = true;
    // @ts-expect-error
    component.keyManager = { activeItem: new MockOptionComponent<Option> };
    // @ts-expect-error
    const mockActiveItem = component.keyManager.activeItem as MockOptionComponent<Option>;

    fixture.debugElement.triggerEventHandler('keydown', { key: 'Enter' } as KeyboardEvent);

    expect(mockActiveItem.toggleSelect).toHaveBeenCalledTimes(1);
  })

  it('should call onChange() when options change', () => {
    // @ts-expect-error
    const onChangeSpy = spyOn(component, 'onChange');
    const mockOption = new MockOptionComponent<MultiSelectOption>();
    const optionsList = new QueryList<MockOptionComponent<MultiSelectOption>>();
    optionsList.reset([ mockOption ]);
    // @ts-expect-error
    component.options = optionsList;

    component.ngAfterContentInit();
    optionsList.get(0)?.selected.emit(mockOption);

    expect(onChangeSpy).toHaveBeenCalledTimes(1);
  })

  it('should call scrollIntoThisOption() when keyManager.change event is triggered', () => {
    const mockOption = new MockOptionComponent<MultiSelectOption>();
    const optionsList = new QueryList<MockOptionComponent<MultiSelectOption>>();
    optionsList.reset([ mockOption ]);
    // @ts-expect-error
    component.options = optionsList;

    // @ts-expect-error
    component.keyManager.change.next(0);

    expect(mockOption.scrollIntoThisOption).toHaveBeenCalledTimes(1);
    expect(mockOption.scrollIntoThisOption).toHaveBeenCalledWith({
      behavior: 'smooth',
      block: 'center'
    });
  })

});
