import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Renderer2, ElementRef } from '@angular/core';

import { OptionComponent } from './option.component';
import { MultiSelectOption } from 'src/app/core/model/multi-select-input';


describe('OptionComponent', () => {
  let component: OptionComponent<MultiSelectOption>;
  let fixture: ComponentFixture<OptionComponent<MultiSelectOption>>;
  let renderer: Renderer2;
  let elementRef: ElementRef;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [OptionComponent],
    }).compileComponents();
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionComponent<MultiSelectOption>);
    component = fixture.debugElement.componentInstance;

    renderer = fixture.componentRef.injector.get<Renderer2>(Renderer2);
    elementRef = fixture.componentRef.injector.get<ElementRef>(ElementRef);

    // @ts-expect-error
    component.renderer = renderer
    // @ts-expect-error
    component.elementRef = elementRef

    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit selected event when clicked', () => {
    spyOn(component.selected, 'emit');
    component.toggleSelect();

    // @ts-expect-error
    expect(component.isSelected).toBe(true);
    expect(component.selected.emit).toHaveBeenCalled();
  });

  it('should set active styles when setActiveStyles() is called', () => {
    component.setActiveStyles();

    // @ts-expect-error
    expect(component.isActive).toBe(true);
  })

  it('should set inactive styles when setInactiveStyles() is called', () => {
    component.setInactiveStyles();

    // @ts-expect-error
    expect(component.isActive).toBe(false);
  })

  it('should set selected if in input value marked as selected', () => {
    component.value = { id: 1, value: 'Option 1', done: true };
    component.propertyIndicatingSelection = 'done';

    component.ngOnInit();

    // @ts-expect-error
    expect(component.isSelected).toBe(true);
  })

  it('should remain unselected if in input value marked as unselected', () => {
    component.value = { id: 1, value: 'Option 1', done: false};
    component.propertyIndicatingSelection = 'done';

    component.ngOnInit();

    // @ts-expect-error
    expect(component.isSelected).toBe(false);
  })

  it('should call selectRootElement() and scrollIntoView() with correct arguments when scrollIntoThisOption() is called', () => {
    const nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    const htmlElement = renderer.selectRootElement(nativeElement, true) as HTMLElement;
    const options: ScrollIntoViewOptions = { behavior: 'smooth', block: 'center' };

    spyOn(renderer, 'selectRootElement').and.callThrough();
    spyOn(htmlElement, 'scrollIntoView').and.callThrough();

    component.scrollIntoThisOption(options);

    expect(renderer.selectRootElement).toHaveBeenCalledWith(nativeElement, true);
    expect(htmlElement.scrollIntoView).toHaveBeenCalledWith(options);
  });
});
