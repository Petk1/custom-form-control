import {
  Component,
  EventEmitter,
  Input,
  Output,
  HostListener,
  HostBinding,
  ChangeDetectionStrategy,
  OnInit,
  ChangeDetectorRef,
  inject,
  ElementRef,
  Renderer2
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Highlightable } from '@angular/cdk/a11y';

@Component({
  selector: 'cc-form-option',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionComponent<T extends object> implements OnInit, Highlightable {
  private cd = inject(ChangeDetectorRef);
  private renderer = inject(Renderer2);
  private elementRef = inject(ElementRef<HTMLElement>)

  @Input({ required: true })
  propertyIndicatingSelection!: string;

  @Input({ required: true })
  value: T | null = null;

  @Input()
  handleAfterTouched: ((value: T | null) => any) | null = null;

  @Output()
  selected = new EventEmitter<OptionComponent<T>>();

  @HostListener('click')
  toggleSelect(): void {
    this.isSelected = !this.isSelected;
    this.handleAfterTouched?.(this.value);
    this.selected.emit(this);
    this.cd.markForCheck();
  }

  @HostBinding('class.selected')
  protected isSelected = false;

  @HostBinding('class.active')
  protected isActive = false;

  @HostBinding('class.disabled')
  disabled = false;

  ngOnInit(): void {
    if(!this.value || !this.propertyIndicatingSelection) return;

    if(this.value[this.propertyIndicatingSelection as keyof Object]){
      this.setSelected();
    }
  }

  setSelected(): void {
    this.isSelected = true;
  }

  setActiveStyles(): void {
    this.isActive = true;
    this.cd.markForCheck();
  }

  setInactiveStyles(): void {
    this.isActive = false;
    this.cd.markForCheck();
  }

  scrollIntoThisOption(options: ScrollIntoViewOptions): void {
    (this.renderer.selectRootElement(this.elementRef.nativeElement, true) as HTMLElement)
    .scrollIntoView(options);
  }

}
