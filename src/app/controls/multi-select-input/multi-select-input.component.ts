import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  DestroyRef,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output,
  QueryList,
  ViewChild,
  inject
} from '@angular/core';
import { trigger, state, style, transition, animate, AnimationEvent } from '@angular/animations';
import { CommonModule } from '@angular/common';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { OptionComponent } from './option/option.component';
import { merge, startWith, switchMap, tap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';

export type MultiSelectInputValue<T extends object> = T[] | null;

enum Key {
  UP = 'ArrowUp',
  DOWN = 'ArrowDown',
  ENTER = 'Enter',
}

@Component({
  selector: 'cc-multi-select-input',
  standalone: true,
  imports: [CommonModule, OverlayModule],
  templateUrl: './multi-select-input.component.html',
  styleUrls: ['./multi-select-input.component.scss'],
  animations: [
    trigger('dropDown', [
      state('void', style({ transform: 'scaleY(0)', opacity: 0 })),
      state('*', style({ transform: 'scaleY(1)', opacity: 1 })),
      transition(':enter', [ animate('270ms cubic-bezier(0, 1.1, .35, 1.1)') ]),
      transition(':leave', [ animate('320ms cubic-bezier(.9, -.55, .8, .8)') ]),
    ])
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MultiSelectInputComponent,
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiSelectInputComponent<T extends object> implements ControlValueAccessor, AfterContentInit {
  private destroyRef = inject(DestroyRef);
  private cd = inject(ChangeDetectorRef);

  @Input()
  listHeight: string = '100px';

  @Output()
  readonly searchChanged = new EventEmitter<string>();

  @Output()
  readonly panelOpened = new EventEmitter<void>();

  @Output()
  readonly panelClosed = new EventEmitter<void>();

  public controlValue: MultiSelectInputValue<T> = null;
  public inputValue: string = '';
  public isOpenPanel: boolean = false;

  protected onChange: (newValue: MultiSelectInputValue<T>) => void = () => {};
  protected onTouched: () => void = () => {};

  private keyManager!: ActiveDescendantKeyManager<OptionComponent<T>>;

  @ViewChild('input')
  inputElement!: ElementRef<HTMLInputElement>;

  @HostBinding('class.disabled')
  disabled = false;

  @HostBinding('attr.tabIndex')
  tabIndex = 1;

  @HostListener('click')
  openPanel(): void {
    this.isOpenPanel = true;
    this.cd.markForCheck();
  }

  closePanel(): void {
    this.isOpenPanel = false;
    this.inputElement.nativeElement.blur();
    this.onTouched();
    this.cd.markForCheck();
  }

  @HostListener('focusout')
  markAsTouched(): void {
    if(this.disabled) return;

    if(this.isOpenPanel){
      this.inputElement.nativeElement.focus();
      return;
    }

    this.onTouched();
    this.cd.markForCheck();
  }

  @HostListener('keydown', ['$event'])
  protected onKeyDown(event: KeyboardEvent): void {
    const key = event.key;

    if(!this.isOpenPanel && key === Key.DOWN){
      this.openPanel();
      return;
    }

    if(!this.isOpenPanel) return;

    if(key === Key.DOWN || key === Key.UP){
      this.keyManager.onKeydown(event);
      return;
    }

    const activeItem = this.keyManager?.activeItem;

    if(key === Key.ENTER){
      activeItem?.toggleSelect();
    }
  }

  @ContentChildren(OptionComponent, { descendants: true })
  options!: QueryList<OptionComponent<T>>;

  writeValue(value: MultiSelectInputValue<T>): void {
    this.controlValue = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cd.markForCheck();
  }

  ngAfterContentInit(): void {
    this.keyManager = new ActiveDescendantKeyManager(this.options).withWrap();
    this.keyManager.change.pipe(
      tap((itemIndex: number) => {
        this.options.get(itemIndex)?.scrollIntoThisOption({
          behavior: 'smooth',
          block: 'center'
        })
      }),
      takeUntilDestroyed(this.destroyRef)
    ).subscribe();

    this.options.changes.pipe(
      startWith<QueryList<OptionComponent<T>>>(this.options),
      switchMap(options => (
        merge(...options.map(({ selected }) => selected))
      )),
      tap(() => this.onChange(this.controlValue)),
      takeUntilDestroyed(this.destroyRef)
    ).subscribe();
  }

  protected onHandleInput(event: Event): void {
    if(!this.isOpenPanel){
      this.openPanel();
    }

    const value = (event.target as HTMLInputElement).value;
    this.searchChanged.emit(value);
  }

  protected onPanelAnimation({ fromState, toState }: AnimationEvent): void {
    if(fromState === 'void' && !toState && this.isOpenPanel){
      this.panelOpened.emit();
    }

    if(toState === 'void' && !fromState && !this.isOpenPanel){
      this.panelClosed.emit();
    }
  }

}
