
import { ChangeDetectionStrategy, Component, computed, signal } from '@angular/core';
import { MultiSelectOption } from '../core/model/multi-select-input';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { MultiSelectInputComponent, MultiSelectInputValue } from '../controls/multi-select-input/multi-select-input.component';
import { OptionComponent } from '../controls/multi-select-input/option/option.component';

@Component({
  selector: 'cc-use-multi-select-input',
  standalone: true,
  imports: [
    UseMultiSelectInputComponent,
    ReactiveFormsModule,
    MultiSelectInputComponent,
    OptionComponent,
    NgFor,
    NgIf,
  ],
  templateUrl: './use-multi-select-input.component.html',
  styleUrls: ['./use-multi-select-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UseMultiSelectInputComponent {
  options: MultiSelectOption[] = [
    new MultiSelectOption(1, 'Coffee'),
    new MultiSelectOption(2, 'Tea'),
    new MultiSelectOption(3, 'Energy drink'),
    new MultiSelectOption(4, 'Water'),
    new MultiSelectOption(5, 'Juice'),
    new MultiSelectOption(6, 'Shake'),
    new MultiSelectOption(7, 'Wine'),
  ];

  customControl: FormControl<MultiSelectInputValue<MultiSelectOption>> = new FormControl(this.options);

  filteredOptions = signal(this.options);

  listHeight = computed(() => {
    let itemHeightPx = 31;
    const numberOfOptions = this.filteredOptions().length;

    if(numberOfOptions) {
      itemHeightPx = numberOfOptions * itemHeightPx + 15;
      return itemHeightPx > 200 ? '200px' : `${itemHeightPx}px`;
    }

    return '35px';
  })

  handleAfterOptionSelected(option: MultiSelectOption | null): void {
    if(option && 'done' in option){
      option.done = !option.done;
    }
  }

  onSearchChanged(inputValue: string): void {
    this.filteredOptions.set(this.options.filter(option => (
      option?.value?.toLowerCase?.()?.includes?.(inputValue?.toLowerCase?.())
    )))
  }

  onPanelOpened(): void {
    console.log('Panel opened');
  }

  onPanelClosed(): void {
    console.log('Panel closed');
  }

  submit(): void {
    console.log(this.customControl.value)
  }
}
